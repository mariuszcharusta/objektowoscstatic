public class Wyswietl {
    public String poleTekstu;
    public static String sciezkaDoPliku="C:\\mojplik.txt";

    public Wyswietl(final String poleTekstu) {
        this.poleTekstu = poleTekstu;
    }
    //metoda statyczna
    static void wyswietlTekst(String tekst){
        System.out.println("Tekst z metody statycznej : " + tekst);
    }
    //nie ma slowa static, nie jest statyczna
    void wyswietlTekstNieStatycznie(String tekst){
        System.out.println("Tekst wyświetlany z metody nie statycznej : " + tekst);
    }

}
